resource "random_uuid" "main" {}

resource "aws_s3_bucket" "bucketf" {
  bucket = "my-tf-bkp-bucket-${random_uuid.main.result}"
  acl    = "private"

  tags = {
    Name        = "My TF Bucket"
    Environment = "Prod"
  }
}