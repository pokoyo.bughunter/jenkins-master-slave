#############################################################################
# VARIABLES
#############################################################################
variable "region_1" {
  type    = string
  default = "us-east-1"
}

variable "mapPublicIP" {
  default = true
}

variable "availabilityZone" {
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "azs" {
  default = "2"
}

variable "vpc_name" {
  type        = string
  default     = "jenkins-ms-vpc-prod"
  description = "Jenkins VPC East Zone"
}

variable "vpc_cidr_range" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet" {
  type = map(string)
  default = {
    "backend" = "Jenkins Backend subnet"
    "bastion" = "Jenkins Bastion subnet"
    "public"  = "Jenkins Public subnet"
  }
}

variable "public_subnets" {
  type    = list(string)
  default = ["10.0.20.0/24", "10.0.21.0/24"]
}

variable "private_subnets" {
  type    = list(string)
  default = ["10.0.30.0/24", "10.0.31.0/24"]
}

variable "bastion_cidr" {
  type = map(string)
  default = {
    "bastion" = "10.0.0.0/24"
  }
}

variable "allIPsCIDRblock" {
  default = "0.0.0.0/0"
}

variable "key_name" {
  default     = "Jenkins-MSKP"
  description = "SSH key name in your AWS account for AWS instances."
}

variable "routetable" {
  type = map(string)
  default = {
    "public"  = "Jenkins Public Route Table"
    "private" = "Jenkins Private Route Table"
  }
}

variable "acl" {
  type = map(string)
  default = {
    "vpc"     = "Jenkins VPC ACL"
    "public"  = "Jenkins Public ACL"
    "private" = "Jenkins Private ACL"
    "bastion" = "Jenkins Bastion ACL"
  }
}

variable "internet-gateway" {
  type    = string
  default = "Jenkins  Internet Gateway"
}

variable "instance_type_list" {
  description = "EC2 Instance Type"
  type        = list(string)
  default     = ["t2.micro", "t2.medium"]
}

variable "instance_type_map" {
  description = "EC2 Instance Type"
  type        = map(string)
  default = {
    "dev"  = "t2.micro",      //0.0116 USD per Hour
    "qa"   = "t2.small",      //0.023 USD per Hour
    "prod" = "t2.medium"      //0.0464 USD per Hour
  }
}

variable "instanceTenancy" {
  default = "default"
}

variable "dnsSupport" {
  default = true
}

variable "dnsHostNames" {
  default = true
}

variable "security_group" {
  type = map(string)
  default = {
    "backend-alb" = "Jenkins Backend ALB Security Group"
    "backend"     = "Jenkins Backend Security Group"
    "bastion"     = "Jenkins Bastion Security Group"
    "public"      = "Jenkins  Public Security Group"
  }
}

variable "security_group_lbl" {
  default = "Jenkins_Access_SG"
}
