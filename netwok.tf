#############################################################################
# RESOURCES
#############################################################################  
# Create Subnets
resource "aws_subnet" "jenkins-ms-backend-subnet" {
  count      = var.azs
  vpc_id     = aws_vpc.vpc-prod.id
  cidr_block = var.private_subnets[count.index]
  #map_public_ip_on_launch = var.mapPublicIP
  availability_zone = data.aws_availability_zones.azs.names[count.index]
  tags = {
    Name        = var.subnet["backend"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_network_acl" "jenkins-ms-backend-acl" {
  vpc_id     = aws_vpc.vpc-prod.id
  subnet_ids = aws_subnet.jenkins-ms-backend-subnet.*.id

  # allow ingress ephemeral ports from all IPs
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.allIPsCIDRblock
    from_port  = 0
    to_port    = 65535
  }

  # allow egress ephemeral ports to all IPs
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.allIPsCIDRblock
    from_port  = 0
    to_port    = 65535
  }
  tags = {
    Name        = var.acl["private"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_network_acl" "jenkins-ms-bastion-acl" {
  vpc_id     = aws_vpc.vpc-prod.id
  subnet_ids = [aws_subnet.jenkins-ms-bastion-subnet.id]

  # allow ingress SSH port 22 from all IPs
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.allIPsCIDRblock
    #from_port  = 22
    #to_port    = 22
    from_port = 0
    to_port   = 65535
  }

  # allow ingress ephemeral ports from VPC subnet IPs
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.vpc_cidr_range
    #cidr_block = var.cidr["vpc"]
    from_port = 1024
    to_port   = 65535
  }

  # allow egress ephemeral ports to all IPs
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.allIPsCIDRblock
    from_port  = 0
    to_port    = 65535
  }
  tags = {
    Name        = var.acl["bastion"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_subnet" "jenkins-ms-bastion-subnet" {
  vpc_id                  = aws_vpc.vpc-prod.id
  cidr_block              = var.bastion_cidr["bastion"]
  map_public_ip_on_launch = var.mapPublicIP
  #availability_zone       = data.aws_availability_zones.azs.names
  availability_zone = var.availabilityZone[1]

  tags = {
    Name        = var.subnet["bastion"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}


resource "aws_security_group" "bastion_sg" {
  provider    = aws.east
  name        = var.security_group["bastion"]
  vpc_id      = aws_vpc.vpc-prod.id
  description = "Allow Traffic"

  ingress {
    description = "Allow from Personal CIDR block"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #FIX
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks      = var.vpc_cidr_range
    #ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = var.acl["bastion"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_lb" "jenkins-ms-backend-alb" {
  name               = "jenkins-ms-backend-alb"
  internal           = false
  load_balancer_type = "application"
  #security_groups    = [aws_security_group.jenkins_alb_sg.id]
  subnets = [for subnet in aws_subnet.jenkins-ms-backend-subnet : subnet.id]

  #enable_deletion_protection = true

  /*
  access_logs {
    bucket  = aws_s3_bucket.lb_logs.bucket
    prefix  = "test-lb"
    enabled = true
  }
  */

  tags = {
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

# ALB Configuration for Backend Target Group
resource "aws_lb_listener" "jenkins-ms-backend-alb" {
  load_balancer_arn = aws_lb.jenkins-ms-backend-alb.arn
  port              = "8082"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.jenkins-ms-backend-tg.arn
  }
}

resource "aws_lb_target_group" "jenkins-ms-backend-tg" {
  name     = "jenkins-ms-backend-tg"
  port     = 8082
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc-prod.id
}

resource "aws_lb_target_group_attachment" "jenkins-ms-backend-tg_attach" {
  count            = var.azs
  target_group_arn = aws_lb_target_group.jenkins-ms-backend-tg.arn
  target_id        = element(aws_instance.jenkins_master.*.id, count.index)
  port             = 8082
}

# Create Internet Gateway
resource "aws_internet_gateway" "jenkins-ms-igw" {
  vpc_id = aws_vpc.vpc-prod.id
  tags = {
    Name = var.internet-gateway
  }
}

# Create Route Tables
resource "aws_route_table" "jenkins-ms-public-route" {
  vpc_id = aws_vpc.vpc-prod.id
  tags = {
    Name        = var.routetable["public"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

# Create Internet route access
resource "aws_route" "jenkins-ms-internet-route" {
  route_table_id         = aws_route_table.jenkins-ms-public-route.id
  destination_cidr_block = var.allIPsCIDRblock # FIX IT - 
  gateway_id             = aws_internet_gateway.jenkins-ms-igw.id
}

resource "aws_security_group" "jenkins_alb_sg" {
  provider    = aws.east
  name        = var.security_group_lbl
  vpc_id      = aws_vpc.vpc-prod.id
  description = "Allow Traffic"

  ingress {
    description = "Allow from Personal CIDR block"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #FIX
  }

  ingress {
    description = "Allow from Personal CIDR block"
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks      = var.vpc_cidr_range
    #ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = var.security_group["backend-alb"]
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}
