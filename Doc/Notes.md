# Project1-CICD-Pipeline-with-Jenkins-Architecting-Jenkins-Pipeline-Scale

1. Use Jenkins to set up a distributed pipeline that will compile and test a Maven project on two different slave nodes respectively.

# MAVEN PROJECTS

- https://gitlab.com/pokoyo.bughunter/simple-java-maven-app



2. There should be three EC2 instances to run the master and two slave nodes.
3. All builds should be triggered and monitored by the master node.
4. Compilation and testing should be done on dedicated slave nodes.



# Documentation 

- https://learn.sandipdas.in/2021/05/23/adding-jenkins-slave-node-on-ubuntu-linux/
- https://www.howtoforge.com/tutorial/ubuntu-jenkins-master-slave/
- https://tomgregory.com/using-jenkins-configuration-as-code-to-setup-aws-slave-agents-automatically/

# Installing

- https://www.jenkins.io/doc/book/installing/


# Other Sources

- https://geekdudes.wordpress.com/2018/01/09/install-packages-to-amazon-virtual-machine-using-terraform/
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
- https://www.bitovi.com/blog/bitops-terraform-ansible
- https://dev.to/mariehposa/how-to-deploy-an-application-to-aws-ec2-instance-using-terraform-and-ansible-3e78