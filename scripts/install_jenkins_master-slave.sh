# UBUNTU LINUX
#!/bin/bash
sleep 30
# sleep until instance is ready
#until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
#  sleep 1
#done

touch /tmp/AWS-INSTALL.log
cat /var/lib/cloud/instance/boot-finished >> /tmp/AWS-INSTALL.log

# install Jenkins 
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null
apt-get update
apt-get install jenkins -y
echo "Installing jenkins" >> /tmp/AWS-INSTALL.log

# Install Java - OpenJDK 11 
apt update
apt install openjdk-11-jre -y
echo "Installing Java" >> /tmp/AWS-INSTALL.log
apt install git -y
echo "Installing Git" >> /tmp/AWS-INSTALL.log
java -version

# Start Jenkins

systemctl enable jenkins
systemctl start jenkins
systemctl status jenkins

cat /var/lib/jenkins/secrets/initialAdminPassword


