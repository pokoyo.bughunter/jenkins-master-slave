#!/bin/bash
sleep 30
# Master Node Changes 
cat /etc/passwd | grep jenkins
usermod -aG sudo jenkins
groups jenkins
passwd jenkins
su - jenkins
mkdir -p ~/.ssh
cd ~/.ssh
#ssh-keyscan -H SLAVE_PRIVATE_IP_ADDRESS_HERE >>/var/lib/jenkins/.ssh/known_hosts
ssh-keyscan -H 172.31.28.44 >>/var/lib/jenkins/.ssh/known_hosts
ssh-keyscan -H 172.31.29.207 >>/var/lib/jenkins/.ssh/known_hosts
chown jenkins:jenkins /var/lib/jenkins/.ssh/known_hosts
chmod 700 /var/lib/jenkins/.ssh/known_hosts