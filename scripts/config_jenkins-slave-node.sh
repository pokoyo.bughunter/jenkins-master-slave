#!/bin/bash
sleep 30
# Slave Node Changes
useradd linux-slave -m -s /bin/bash
cat /etc/passwd | grep slave
usermod -aG sudo linux-slave
passwd linux-slave
su - linux-slave
ssh-keygen -t rsa -N "" -f /home/linux-slave/.ssh/id_rsa
#cd /home/linux-slave/.ssh/
cat /home/linux-slave/.ssh/id_rsa.pub > /home/linux-slave/.ssh/authorized_keys
chmod 700 /home/linux-slave/.ssh/authorized_keys

