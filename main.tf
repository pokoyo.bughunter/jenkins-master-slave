#############################################################################
# PROVIDERS
#############################################################################
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.16.0"
    }
  }
}

provider "aws" {
  region = var.region_1
  alias  = "east"
  profile = "default"
}

#############################################################################
# DATA SOURCES
#############################################################################

data "aws_caller_identity" "infra" {
  provider = aws.east
}

data "aws_availability_zones" "azs" {
  provider = aws.east
}

data "template_file" "user_data" {
  template = file("scripts/install_jenkins_master-slave.sh")
}

# Get latest Ubuntu Linux Focal Fossa 20.04 AMI
data "aws_ami" "ubuntu-linux-2004" {
  provider    = aws.east
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#############################################################################
# OUTPUTS
#############################################################################
output "vpc_id" {
  value = aws_vpc.vpc-prod.id
}

output "private_subnets" {
  description = "The IDs of the private subnets as list"
  value       = ["${aws_subnet.jenkins-ms-backend-subnet[*].id}"]
}

