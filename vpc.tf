#############################################################################
# RESOURCES
#############################################################################  
# Create VPC
resource "aws_vpc" "vpc-dev" {
  cidr_block           = var.vpc_cidr_range
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames

  tags = {
    name        = var.vpc_name
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }

}

resource "aws_vpc" "vpc-qa" {
  cidr_block           = var.vpc_cidr_range
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames

  tags = {
    name        = var.vpc_name
    Environment = "Quality Assurance (QA)"
    Autor       = "Kennedy Sanchez @Sappotech"
  }

}

resource "aws_vpc" "vpc-prod" {
  cidr_block = var.vpc_cidr_range
  #azs = var.azs
  #private_subnets = var.private_subnets
  #public_subnets = var.public_subnets
  #instance_tenancy     = var.instanceTenancy
  #enable_dns_support   = var.dnsSupport
  #enable_dns_hostnames = var.dnsHostNames

  tags = {
    name        = var.vpc_name
    Environment = "Production (PROD)"
    Autor       = "Kennedy Sanchez @Sappotech"
  }

}