# TODO: BUG FIX - Need to apply DRY(Don't Repeat Yourself)

resource "aws_instance" "jenkins_master" {
  #count                       = var.azs
  provider                    = aws.east
  ami                         = data.aws_ami.ubuntu-linux-2004.id
  instance_type               = var.instance_type_map["dev"]
  key_name                    = var.key_name
  monitoring                  = true
  associate_public_ip_address = false
  vpc_security_group_ids      = [aws_security_group.jenkins_alb_sg.id]
  subnet_id                   = aws_subnet.jenkins-ms-backend-subnet.*.id[0] # FIX DUP
  user_data                   = file("scripts/install_jenkins_master-slave.sh")
  availability_zone           = data.aws_availability_zones.azs.names[0]


  tags = {
    Name        = "Jenkins Master"
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_instance" "jenkins_slave" {
  provider                    = aws.east
  ami                         = data.aws_ami.ubuntu-linux-2004.id
  instance_type               = var.instance_type_map["dev"]
  key_name                    = var.key_name
  monitoring                  = true
  associate_public_ip_address = false
  vpc_security_group_ids      = [aws_security_group.jenkins_alb_sg.id]
  subnet_id                   = aws_subnet.jenkins-ms-backend-subnet.*.id[count.index] # FIX DUP
  user_data                   = file("scripts/install_jenkins_master-slave.sh")
  count                       = 2

  tags = {
    Name        = "Jenkins Slave-${count.index}"
    Environment = "Production"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_instance" "jenkins-ms-bastion" {
  #count                  = var.azs
  provider                    = aws.east
  ami                         = data.aws_ami.ubuntu-linux-2004.id
  instance_type               = var.instance_type_map["dev"]
  key_name                    = var.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  subnet_id                   = aws_subnet.jenkins-ms-bastion-subnet.id
  #subnet_id  = each.key
  #for_each                    = toset(data.aws_availability_zones.azs.names)   # TESTING DYNAMIC
  #availability_zone           = each.key
  availability_zone      = data.aws_availability_zones.azs.names[1]
  tags = {
    Name  = "Bastion Host" #-${each.key}"
    Role  = "bastion"
    Autor = "Kennedy Sanchez @Sappotech"
  }
}

